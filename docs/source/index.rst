.. MinecraftElements documentation master file, created by
   sphinx-quickstart on Tue Jun 22 17:54:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MinecraftElements's documentation!
=============================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. include:: license.rst
.. include:: overview.rst
.. include:: installation.rst
.. include:: usage.rst
.. include:: code.rst
.. include:: changelog.rst

Indices and tables
==================



